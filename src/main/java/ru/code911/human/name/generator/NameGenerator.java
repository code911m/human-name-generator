/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.code911.human.name.generator;

import java.util.Random;

/**
 * Генератор человеческих имен.
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class NameGenerator {

    private static final String ARG_COUNT = "--count";
    private static final String ARG_SEX = "--sex";
    private static final String SEX_MALE = "male";
    private static final String SEX_FEMALE = "female";
    private static final String USAGE = "USAGE: human-name-generator.jar " + ARG_COUNT + " <count of names> " + ARG_SEX
            + " <" + SEX_MALE + "|" + SEX_FEMALE + ">";

    public static void main(String[] args) {
        if ((args.length < 4)
                || !(((ARG_COUNT.equals(args[0])) && (ARG_SEX.equals(args[2])))
                || ((ARG_COUNT.equals(args[2])) && (ARG_SEX.equals(args[0]))))) {
            System.err.println(USAGE);
            System.exit(1);
        }
        try {
            // count of names
            int count;
            if (ARG_COUNT.equals(args[0])) {
                count = Integer.valueOf(args[1]);
            } else {
                count = Integer.valueOf(args[3]);
            }
            // sex
            String sex;
            if (ARG_SEX.equals(args[0])) {
                sex = args[1];
            } else {
                sex = args[3];
            }
            if ((!SEX_MALE.equals(sex)) && (!SEX_FEMALE.equals(sex))) {
                System.err.println(USAGE);
                System.exit(1);
            }
            // gen and system out
            for (int i = 0; i < count; i++) {
                System.out.println(getRandomName("male".equals(sex)));
            }
        } catch (NumberFormatException ex) {
            System.err.println(USAGE);
            System.exit(2);
        }
    }

    /**
     * Получить случайный элемент массива
     *
     * @param array массив
     * @return
     */
    private static String getRandomElement(final String[] array) {
        int idx = new Random().nextInt(array.length);
        return array[idx];
    }

    /**
     * Сгенерировать случайную фамилию
     *
     * @param male пол
     * @return
     */
    public static String getRandomName(boolean male) {
        StringBuilder sb = new StringBuilder();
        if (male) {
            sb
                    .append(getRandomElement(Dictionary.MALE_LAST_NAMES))
                    .append(" ")
                    .append(getRandomElement(Dictionary.MALE_FIRST_NAMES))
                    .append(" ")
                    .append(getRandomElement(Dictionary.MALE_MIDDLE_NAMES));
        } else {
            sb
                    .append(getRandomElement(Dictionary.FEMALE_LAST_NAMES))
                    .append(" ")
                    .append(getRandomElement(Dictionary.FEMALE_FIRST_NAMES))
                    .append(" ")
                    .append(getRandomElement(Dictionary.FEMALE_MIDDLE_NAMES));
        }
        return sb.toString();
    }
}
